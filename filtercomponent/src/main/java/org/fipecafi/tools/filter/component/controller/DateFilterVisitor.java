package org.fipecafi.tools.filter.component.controller;

import java.util.Calendar;
import java.util.logging.Logger;

import org.fipecafi.tools.filter.DateFilter;
import org.fipecafi.tools.filter.FilterVisitorAdapter;

public class DateFilterVisitor extends FilterVisitorAdapter {
	private String choice;
	private static final Logger logger = Logger.getLogger(DateFilterVisitor.class.getName());
	public DateFilterVisitor(String choice){
		//this.choice = choice;
	}
	@Override
	public void visit(DateFilter filter) {
		Calendar endCal = Calendar.getInstance();
		Calendar startCal = null;
		if(filter.getChoice()==null || "".equals(filter.getChoice().trim()))
			return;
		logger.info("Choice: "+filter.getChoice()+" for filter :"+filter.getFilterName());
		System.out.println("Choice: "+filter.getChoice()+" for filter :"+filter.getFilterName());
		this.choice = filter.getChoice();
		if(choice.equalsIgnoreCase("withinButton")){
			String withinChoice = filter.getWithinChoice();
			logger.info("WithinChoice: "+withinChoice);
			startCal = Calendar.getInstance();
			if(withinChoice.equals("days")){
				startCal.add(Calendar.DAY_OF_YEAR, filter.getWithinVal()*-1);
			}else if(withinChoice.equals("weeks")){
				startCal.add(Calendar.WEEK_OF_YEAR, filter.getWithinVal()*-1);
			}else if(withinChoice.equals("months")){
				startCal.add(Calendar.MONTH, filter.getWithinVal()*-1);
			}else if(withinChoice.equals("years	")){
				startCal.add(Calendar.YEAR, filter.getWithinVal()*-1);
			}
		}else if(choice.equalsIgnoreCase("moreThanButton")){
			String moreThanChoice = filter.getMoreThanChoice();
			logger.info("MoreThanChoice: "+moreThanChoice);
			startCal = Calendar.getInstance();
			if(moreThanChoice.equals("seconds")){
				startCal.add(Calendar.SECOND, filter.getMoreThanVal()*-1);
			}else if(moreThanChoice.equals("minutes")){
				startCal.add(Calendar.MINUTE, filter.getMoreThanVal()*-1);
			}if(moreThanChoice.equals("hours")){
				startCal.add(Calendar.HOUR_OF_DAY, filter.getMoreThanVal()*-1);
			}
		}else if(choice.equalsIgnoreCase("betweenButton")){
			logger.info("StartDate: "+filter.getStartDate()+", EndDate: "+filter.getEndDate());
			startCal = Calendar.getInstance();
			startCal.setTime(filter.getStartDate());
			endCal.setTime(filter.getEndDate());
		}else if(choice.equalsIgnoreCase("rangeButton")){
			String startRange = filter.getStartRange();
			String endRange = filter.getEndRange();
			logger.info("StartRange: "+startRange+", EndRange: "+endRange);
			String startWeekNum = String.valueOf(startRange.toCharArray()[1]);
			String startDayNum = String.valueOf(startRange.toCharArray()[3]);
			logger.info("Start Details; "+startWeekNum+", "+startDayNum);
			String endWeekNum = String.valueOf(endRange.toCharArray()[0]);
			String endDayNum = String.valueOf(endRange.toCharArray()[2]);
			logger.info("End Details; "+endWeekNum+", "+endDayNum);
			startCal = Calendar.getInstance();
			startCal.add(Calendar.WEEK_OF_YEAR, Integer.parseInt(startWeekNum)*-1);
			startCal.add(Calendar.DAY_OF_YEAR, Integer.parseInt(startDayNum)*-1);
			
			endCal.add(Calendar.WEEK_OF_YEAR, Integer.parseInt(endWeekNum));
			endCal.add(Calendar.DAY_OF_YEAR, Integer.parseInt(endDayNum));
		}
		filter.setStartCal(startCal);
		filter.setEndCal(endCal);
		logger.info("StartCal: "+startCal.getTime()+", EndCal: "+endCal.getTime());
	}
	
}
