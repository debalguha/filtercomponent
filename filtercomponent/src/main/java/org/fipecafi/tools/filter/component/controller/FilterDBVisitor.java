package org.fipecafi.tools.filter.component.controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.fipecafi.tools.filter.AbstractFilterVisitor;
import org.fipecafi.tools.filter.DateFilter;
import org.fipecafi.tools.filter.Option;
import org.fipecafi.tools.filter.OptionFilter;
import org.fipecafi.tools.filter.QueryFilter;

public class FilterDBVisitor extends AbstractFilterVisitor{
	private Connection conn;
	public FilterDBVisitor(Connection conn){
		this.conn = conn;
	}
	@Override
	public void visitQueryFilter(QueryFilter filter) {
		String query = filter.getQuery();
		ResultSet rs = null;
		try{
			Statement stmt = conn.createStatement();
			rs = stmt.executeQuery(query);
			List<Option> opts = new ArrayList<Option>();
			while(rs.next()){
				Option option = new Option();
				option.setId(rs.getInt(1));
				option.setName(rs.getString(2));
				opts.add(option);
			}
			conn.commit();
			filter.setOptions(opts);
		}catch(Exception e){
			try {
				if(conn!=null)
					conn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		} finally{
			try{
				conn.close();
			}catch(Exception e){
				e.printStackTrace();
			}
		}
	}
	@Override
	public void visit(DateFilter filter) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void visitOptionFilter(OptionFilter filter) {
		// TODO Auto-generated method stub
		
	}
}
