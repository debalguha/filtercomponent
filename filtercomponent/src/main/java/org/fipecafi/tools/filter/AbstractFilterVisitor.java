package org.fipecafi.tools.filter;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public abstract class AbstractFilterVisitor implements FilterVisitor {

	@Override
	public void visit(QueryFilter filter) {
		visitQueryFilter(filter);
		visitGeneric(filter);
	}

	@Override
	public void visit(OptionFilter filter) {
		visitGeneric(filter);
		visitOptionFilter(filter);
	}

	private void visitGeneric(OptionFilter filter){
		Collection<Option> options = filter.getOptions();
		int []values = new int[options.size()];
		Map<Integer, String> optionMap = new HashMap<Integer, String>(options.size());
		int i=0;
		for(Option option : options){
			values[i] = option.getId();
			optionMap.put(option.getId(), option.getName());
			i++;
		}
		filter.setOptionMap(optionMap);
		filter.setValues(values);
	}
	
	public abstract void visitQueryFilter(QueryFilter filter);
	public abstract void visitOptionFilter(OptionFilter filter);

}
