package org.fipecafi.tools.filter.component.controller;

import java.io.InputStream;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.fipecafi.tools.filter.DateFilter;
import org.fipecafi.tools.filter.IFilter;
import org.fipecafi.tools.filter.OptionFilter;

@ManagedBean
@SessionScoped
public class IndexController {
	//@ManagedProperty(value="#{filterController}")
	private FilterController filterController;

	@PostConstruct
	public void init(){
		filterController = new FilterController();
		((HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(false)).setAttribute("filterController", filterController);
		/*InputStream is;
		try {
			
			is = FacesContext.getCurrentInstance().getExternalContext().getResourceAsStream("/WEB-INF/filter.xml");
			if (is != null)
				filterController.init(is);
			else
				System.out.println("Input stream is null!!");
		} catch (Exception e) {
			// e.printStackTrace();
			if (!Boolean.getBoolean("junit"))
				throw new RuntimeException(e);
		}*/
	}
	public void setFilterController(FilterController filterController) {
		this.filterController = filterController;
	}
	public String getAllapplicableFilterValues(){
		List<IFilter> filters = filterController.getAllApplicableFilters();
		StringBuilder strBuilder = new StringBuilder();
		for(IFilter filter : filters){
			StringBuilder childBuilder = new StringBuilder(filter.getFilterName().concat(":"));
			if (!filter.getType().equals("DATE")) {
				int[] values = ((OptionFilter) filter).getValues();
				if (values != null) {
					int i = 0;
					for (int value : values) {
						childBuilder.append(value);
						if (i < values.length - 1)
							childBuilder.append(",");
						i++;
					}
				}
			} else {
				Date startDate = null;
				if (((DateFilter) filter).getStartCal() != null)
					startDate = ((DateFilter) filter).getStartCal().getTime();
				Date endDate = null;
				if (((DateFilter) filter).getEndCal() != null)
					endDate = ((DateFilter) filter).getEndCal().getTime();
				;
				if (startDate != null && endDate != null) {
					childBuilder.append(startDate.getTime()).append(",").append(endDate.getTime());
				}
			}
			childBuilder.append("|");
			strBuilder.append(childBuilder.toString());
		}
		return strBuilder.toString();
	}
}
