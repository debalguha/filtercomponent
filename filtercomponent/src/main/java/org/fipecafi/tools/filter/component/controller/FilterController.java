package org.fipecafi.tools.filter.component.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.fipecafi.tools.filter.DateFilter;
import org.fipecafi.tools.filter.Filter;
import org.fipecafi.tools.filter.FilterXMLParser;
import org.fipecafi.tools.filter.IFilter;
import org.fipecafi.tools.filter.OptionFilter;
import org.fipecafi.tools.filter.demo.servlet.DerbyUtil;

@ManagedBean(name = "filterController")
@RequestScoped
public class FilterController {
	// private Map<String, FilterWrapper> filterConfig;
	private DateFilterVisitor dateFilterVisitor;
	private Map<String, IFilter> filterMap;
	private List<IFilter> defaultOptionFilters;
	private List<IFilter> defaultDateFilters;
	private List<IFilter> moreDateFilters;
	private List<IFilter> moreOptionFilters;
	private List<IFilter> moreFilters;
	/*private List<IFilter> chosenMoreOptionFilters;
	private List<IFilter> chosenMoreDateFilters;*/
	private List<String> chosenMoreFilters;
	private String dateFilterName;
	private int[] dummyVal = new int[]{1};
	private String choice;
	private String filterState;
	private String sessionAttrName;
	public FilterController() {
		defaultOptionFilters = new ArrayList<IFilter>();
		defaultDateFilters = new ArrayList<IFilter>();
		moreOptionFilters = new ArrayList<IFilter>();
		moreDateFilters = new ArrayList<IFilter>();
		/*chosenMoreOptionFilters = new ArrayList<IFilter>();
		chosenMoreDateFilters = new ArrayList<IFilter>();*/
		chosenMoreFilters = new ArrayList<String>();
		moreFilters = new ArrayList<IFilter>();
		/*InputStream is;
		try {
			is = FacesContext.getCurrentInstance().getExternalContext().getResourceAsStream("/WEB-INF/filter.xml");
			if(is!=null)
				init(is);
			else
				System.out.println("Input stream is null!!");			
		} catch (RuntimeException e) {
			//e.printStackTrace();
			if(!Boolean.getBoolean("junit"))
				throw e;
		}*/
	}
	@SuppressWarnings("unchecked")
	@PostConstruct
	public void init() throws RuntimeException {
		System.out.println("Postconstruct invoked.");
		if(sessionAttrName!=null){
			filterMap = (Map<String, IFilter>)((HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(false)).getAttribute(sessionAttrName);
			return;
		}
		Map<String, Object> reqMap = FacesContext.getCurrentInstance().getExternalContext().getRequestMap();
		System.out.println("ReqMap size: "+reqMap!=null?reqMap.size():0);
		String configFilePath = null;
		for(String str : reqMap.keySet()){
			System.out.println("Request param: "+str);
			if(str.contains("configFilePath")){
				configFilePath = reqMap.get(str).toString();
				break;
			}
		}
		InputStream is;
		try {
			is = new FileInputStream(new File(configFilePath));
			init(is);
		} catch (FileNotFoundException e) {
			throw new RuntimeException(e);
		}
		
	}
	
	public void init(InputStream is) throws RuntimeException{
		System.out.println("Initializing..");
		try {
			filterMap = FilterXMLParser.filterMap(FilterXMLParser.marshallXmlConfig(is).getFilters());
			createCollections();
			String guid = UUID.randomUUID().toString();
			((HttpSession)((HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest()).getSession()).setAttribute(guid, filterMap);
			this.sessionAttrName = guid;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	public void initForTest(InputStream is) throws RuntimeException{
		System.out.println("Initializing..");
		try {
			filterMap = FilterXMLParser.filterMap(FilterXMLParser.marshallXmlConfig(is).getFilters());
			createCollections();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}	
	
	private void createCollections() throws Exception{
		FilterDBVisitor visitor = new FilterDBVisitor(DerbyUtil.getConnection());
		for (IFilter filter : filterMap.values()){
			filter.accept(visitor);
			if(filter.getType().equalsIgnoreCase("DATE")){
				if(filter.isDefaultAttr())
					defaultDateFilters.add(filter);
				else{
					moreDateFilters.add(filter);
					moreFilters.add(filter);
				}
			}else{
				if(filter.isDefaultAttr())
					defaultOptionFilters.add(filter);
				else{
					moreOptionFilters.add(filter);
					moreFilters.add(filter);
				}
			}
		}
	}
	
	public List<IFilter> getChosenMoreOptionFilters() {
		List<IFilter> chosenMoreOptionFilters = new ArrayList<IFilter>();
		for(IFilter filter : moreFilters){
			if(chosenMoreFilters.contains(filter.getFilterName())){
				if(!filter.getType().equals("DATE"))
					chosenMoreOptionFilters.add(filter);
			}
		}
		return chosenMoreOptionFilters;
	}
	
	public List<IFilter> getChosenMoreDateFilters() {
		List<IFilter> chosenMoreDateFilters = new ArrayList<IFilter>();
		for(IFilter filter : moreFilters){
			if(chosenMoreFilters.contains(filter.getFilterName())){
				if(filter.getType().equals("DATE"))
					chosenMoreDateFilters.add(filter);
			}
		}		
		return chosenMoreDateFilters;
	}	
	
	public int getDefaultMenuCount() {
		System.out.println("Default Menu count: "+(defaultOptionFilters.size() + defaultDateFilters.size() +1));
		return defaultOptionFilters.size() + defaultDateFilters.size() +1;
	}
	
	public Collection<String> getFilterNames() {
		return filterMap.keySet();
	}

	public String getChoice() {
		return choice;
	}

	public void setChoice(String choice) {
		this.choice = choice;
	}

	public Map<String, IFilter> getFilterMap() {
		return filterMap;
	}

	public List<IFilter> getMoreDateFilters() {
		//System.out.println("Invoking moreCheckBoxChecked from getMoreDateFilters:: ");
		moreCheckboxChecked();
		return moreDateFilters;
	}

	public List<IFilter> getMoreOptionFilters() {
		//System.out.println("Invoking moreCheckBoxChecked from getMoreOptionFilters:: ");
		moreCheckboxChecked();
		return moreOptionFilters;
	}


	public List<IFilter> getDefaultOptionFilters() {
		return defaultOptionFilters;
	}

	public List<IFilter> getDefaultDateFilters() {
		return defaultDateFilters;
	}

	public List<String> getChosenMoreFilters() {
		return chosenMoreFilters;
	}

	public void setChosenMoreFilters(List<String> chosenMoreFilters) {
		this.chosenMoreFilters = chosenMoreFilters;
	}

	public List<IFilter> getMoreFilters() {
		return moreFilters;
	}

	public void setMoreFilters(List<IFilter> moreFilters) {
		this.moreFilters = moreFilters;
	}

	public int[] getDummyVal() {
		return dummyVal;
	}

	public void setDummyVal(int[] dummyVal) {
		this.dummyVal = dummyVal;
	}

	public String getDateFilterName() {
		return dateFilterName;
	}

	public void setDateFilterName(String dateFilterName) {
		this.dateFilterName = dateFilterName;
	}
	
	public void submitDate(){
		System.out.println("I am called!!"+dateFilterName);
		adjustDates();
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"Date filter updated successfully!", "Filter has been updated with specified value."));
	}

	private void adjustDates(){
		for(IFilter filter : filterMap.values()){
			if(filter.getType().equals("DATE")){
				System.out.println(filter.getFilterName()+" choice - "+((DateFilter)filter).getChoice());
				dateFilterVisitor = new DateFilterVisitor(choice);
				filter.accept(dateFilterVisitor);
			}
		}
	}
	
	public void testSubmit(){
		for(IFilter filter : filterMap.values()){
			if(!filter.getType().equals("DATE")){
				int [] values = ((OptionFilter)filter).getValues();
				if(values!=null){
					for(int value : values)
						System.out.print(value+",");
					System.out.println(filter.getFilterName());
				}
			}
		}
	}
	
	public String getFilterState(){
		StringBuilder strbuilder = new StringBuilder();
		System.out.println("Building filter state.");
		adjustDates();
		System.out.println("Dates adjusted.");
		for(IFilter filter : filterMap.values()){
			if(!((Filter)filter).isApplicable()){
				System.out.println("Filter "+filter.getFilterName()+" is not applicable.");
				continue;
			}
			StringBuilder childBuilder = new StringBuilder(filter.getFilterName().concat(":"));
			if(!filter.getType().equals("DATE")){
				int [] values = ((OptionFilter)filter).getValues();
				if(values!=null){
					int i=0;
					for(int value : values){
						childBuilder.append(value);
						if(i<values.length-1)
							childBuilder.append(",");
						i++;
					}
				}
			}else{
				Date startDate = null;
				if(((DateFilter)filter).getStartCal()!=null)
					startDate = ((DateFilter)filter).getStartCal().getTime();
				Date endDate = null;
				if(((DateFilter)filter).getEndCal()!=null)
					endDate = ((DateFilter)filter).getEndCal().getTime();;
				if(startDate!=null && endDate!=null){
					childBuilder.append(startDate.getTime()).append(",").append(endDate.getTime());
				}
			}
			childBuilder.append("|");
			strbuilder.append(childBuilder.toString());
		}
		System.out.println("returning: "+strbuilder.toString());
		this.filterState = strbuilder.toString();
		return filterState;
	}
	
	public List<IFilter> getAllApplicableFilters(){
		List<IFilter> filters = new ArrayList<IFilter>();
		for(IFilter filter : filterMap.values()){
			if(filter.isApplicable())
				filters.addAll(defaultOptionFilters);
		}
		return filters;
	}
	
	public void moreCheckboxChecked(){
		//System.out.println("Chosen More Filters: "+chosenMoreFilters);
		for(String filterName : chosenMoreFilters){
			((Filter)filterMap.get(filterName)).setApplicable(true);
		}
	}
	
	public void setFilterState(String filterState) {
		this.filterState = filterState;
	}
	public String getSessionAttrName() {
		return sessionAttrName;
	}
	public void setSessionAttrName(String sessionAttrName) {
		this.sessionAttrName = sessionAttrName;
	}
}
