package org.fipecafi.tools.filter;

import java.util.Collection;
import java.util.List;
import java.util.Map;


public class OptionFilter extends Filter {
	protected List<Option> options;
	protected Map<Integer, String> optionMap;
	protected int[] values;
	public OptionFilter(FilterPojo filterPojo){
		this.options = filterPojo.getOptions();
		this.defaultAttr = filterPojo.isDefaultAttr();
		this.filterName = filterPojo.getFilterName();
		this.type = filterPojo.getType();
	}
	public Collection<Option> getOptions() {
		return options;
	}
	public void setOptions(List<Option> options) {
		this.options = options;
	}
	@Override
	public void accept(FilterVisitor visitor) {
		visitor.visit(this);
	}
	public int[] getValues() {
		return values;
	}
	public void setValues(int[] values) {
		System.out.print(filterName+" - ");
		for(int value : values)
			System.out.print(value);
		System.out.println();
		this.values = values;
	}
	public String getFilterText(){
		try {
			if(options.size() == values.length)
				return "All";
			StringBuilder strBuilder = new StringBuilder();
			int count = 0;
			for(Integer value : values){
				String name = optionMap.get(value);
				strBuilder.append(name);
				if(count<(values.length-1))
					strBuilder.append(",");
			}
			String retString = null;
			if(strBuilder.length()>20)
				retString = strBuilder.subSequence(0, 20).toString().concat("...");
			else
				retString = strBuilder.toString();
			return retString;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	public Map<Integer, String> getOptionMap() {
		return optionMap;
	}
	public void setOptionMap(Map<Integer, String> optionMap) {
		this.optionMap = optionMap;
	}
}
