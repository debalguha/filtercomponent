package org.fipecafi.tools.filter;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

public class FilterXMLParser {
	public static Filters marshallXmlConfig(InputStream configFile) throws Exception{
		JAXBContext ctx = JAXBContext.newInstance(Filters.class);
		Unmarshaller unmarshaller = ctx.createUnmarshaller();
		Filters filters = (Filters)unmarshaller.unmarshal(configFile);
		return filters;
	}
	public static Map<String, IFilter> filterMap(List<IFilter> filters){
		Map<String, IFilter> filterMap = new HashMap<String, IFilter>();
		for(IFilter filter : filters)
			filterMap.put(filter.getFilterName(), filter);
		return filterMap;
	}
}
