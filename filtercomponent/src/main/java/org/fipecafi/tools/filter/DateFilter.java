package org.fipecafi.tools.filter;

import java.util.Calendar;
import java.util.Date;


public class DateFilter extends Filter {
	private Calendar startCal;
	private Calendar endCal;
	private Date startDate;
	private Date endDate;
	private String choice;
	private int withinVal;
	private String withinChoice;
	private int moreThanVal;
	private String moreThanChoice;
	private String startRange;
	private String endRange;
	public DateFilter(FilterPojo filterPojo){
		this.defaultAttr = filterPojo.isDefaultAttr();
		this.filterName = filterPojo.getFilterName();
		this.type = filterPojo.getType();
	}
	public DateFilter(){
		
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		System.out.println("Set startdate invoked for "+filterName+", value:: "+startDate);
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		System.out.println("Set enddate invoked for "+filterName+", value:: "+endDate);
		this.endDate = endDate;
	}
	@Override
	public void accept(FilterVisitor visitor) {
		visitor.visit(this);
	}
	public int getWithinVal() {
		return withinVal;
	}
	public void setWithinVal(int withinVal) {
		this.withinVal = withinVal;
	}
	public String getWithinChoice() {
		return withinChoice;
	}
	public void setWithinChoice(String withinChoice) {
		this.withinChoice = withinChoice;
	}
	public int getMoreThanVal() {
		return moreThanVal;
	}
	public void setMoreThanVal(int moreThanVal) {
		this.moreThanVal = moreThanVal;
	}
	public String getMoreThanChoice() {
		return moreThanChoice;
	}
	public void setMoreThanChoice(String moreThanChoice) {
		this.moreThanChoice = moreThanChoice;
	}
	public String getStartRange() {
		return startRange;
	}
	public void setStartRange(String startRange) {
		this.startRange = startRange;
	}
	public String getEndRange() {
		return endRange;
	}
	public void setEndRange(String endRange) {
		this.endRange = endRange;
	}
	public String getChoice() {
		return choice;
	}
	public void setChoice(String choice) {
		this.choice = choice;
	}
	public Calendar getStartCal() {
		return startCal;
	}
	public void setStartCal(Calendar startCal) {
		this.startCal = startCal;
	}
	public Calendar getEndCal() {
		return endCal;
	}
	public void setEndCal(Calendar endCal) {
		this.endCal = endCal;
	}
}
