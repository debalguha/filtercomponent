package org.fipecafi.tools.filter;

public abstract class Filter implements IFilter{
	protected boolean defaultAttr;
	protected String filterName;
	protected String type;
	protected boolean applicable;
	
	public boolean isDefaultAttr() {
		return defaultAttr;
	}
	public void setDefaultAttr(boolean defaultAttr) {
		this.defaultAttr = defaultAttr;
	}
	public String getFilterName() {
		return filterName;
	}
	public void setFilterName(String filterName) {
		this.filterName = filterName;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public boolean isApplicable(){
		//System.out.println(filterName+" - isApplicable: "+this.applicable);
		return applicable;
	}
	public void setApplicable(boolean applicable) {
		this.applicable = applicable;
		//System.out.println(filterName+" - setApplicable: "+this.applicable);
	}
	public boolean getApplicable(){
		//System.out.println(filterName+" - getApplicable: "+this.applicable);
		return applicable;
	}
	public String getStyleClass(){
		if(this.applicable)
			return "display:bock;";
		else
			return "display:none";
	}	
}
