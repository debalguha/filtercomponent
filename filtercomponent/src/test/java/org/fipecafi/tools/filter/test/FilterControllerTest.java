package org.fipecafi.tools.filter.test;

import static org.junit.Assert.assertEquals;

import java.io.InputStream;
import java.util.Collection;
import java.util.Map;

import org.fipecafi.tools.filter.IFilter;
import org.fipecafi.tools.filter.Option;
import org.fipecafi.tools.filter.OptionFilter;
import org.fipecafi.tools.filter.component.controller.FilterController;
import org.fipecafi.tools.filter.demo.servlet.DerbyUtil;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class FilterControllerTest {
	InputStream is = null;
	static{
		DerbyUtil.initDerby();
	}
	@Before
	public void setUp() throws Exception {
		System.setProperty("junit", "true");
		is = Thread.currentThread().getContextClassLoader().getResourceAsStream("filter.xml");
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetMenuCount() throws Exception {
		FilterController controller = new FilterController();
		controller.initForTest(is);
		assertEquals(4, controller.getDefaultMenuCount());
		assertEquals(2, controller.getDefaultOptionFilters().size());
		assertEquals(1, controller.getDefaultDateFilters().size());
		assertEquals(1, controller.getMoreDateFilters().size());
		assertEquals(1, controller.getMoreOptionFilters().size());
	}

	@Test
	public void testGetFilterNames() throws Exception {
		FilterController controller = new FilterController();
		controller.initForTest(is);
		assertEquals(5,controller.getFilterNames().size());
	}
	
	@Test
	public void testCheckFilterOptions() throws Exception {
		FilterController controller = new FilterController();
		controller.initForTest(is);
		Collection<IFilter> filters = controller.getFilterMap().values();
		for(IFilter filter : filters){
			if(filter instanceof OptionFilter){
				for(Option option : ((OptionFilter) filter).getOptions())
					System.out.println(option);
			}
		}
	}
	
	@Test
	public void testFilterText() throws Exception{
		FilterController controller = new FilterController();
		controller.initForTest(is);
		Map<Integer, String> optionMap = ((OptionFilter)controller.getFilterMap().get("Location")).getOptionMap();
		int []values = new int[]{1910,180,300};
		StringBuilder strBuilder = new StringBuilder();
		int count = 0;
		for(Integer value : values){
			String name = optionMap.get(value);
			strBuilder.append(name);
			if(count<(values.length-1))
				strBuilder.append(",");
		}
		String retString = null;
		if(strBuilder.length()>20)
			retString = strBuilder.subSequence(0, 20).toString().concat("...");
		else
			retString = strBuilder.toString();
		System.out.println(retString);
	}
}
