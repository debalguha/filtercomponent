package org.fipecafi.tools.filter.test;

import java.util.Calendar;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PatternTest {
	private static final Logger logger = Logger.getLogger(PatternTest.class.getName());
	public static void main(String args[]){
		String patternStr = "^-[0-9]w[0-9]d$";
		Pattern pattern = Pattern.compile(patternStr);
		Matcher matcher = pattern.matcher("-3w4d");
		logger.info(String.valueOf(matcher.matches()));
		String startRange = "-3w4d";
		String endRange = "3w4d";

		logger.info("StartRange: "+startRange+", EndRange: "+endRange);
		String startWeekNum = String.valueOf(startRange.toCharArray()[1]);
		String startDayNum = String.valueOf(startRange.toCharArray()[3]);
		logger.info("Start Details; "+startWeekNum+", "+startDayNum);
		String endWeekNum = String.valueOf(endRange.toCharArray()[0]);
		String endDayNum = String.valueOf(endRange.toCharArray()[2]);
		logger.info("End Details; "+endWeekNum+", "+endDayNum);
		
		Calendar startCal = Calendar.getInstance();
		Calendar endCal = Calendar.getInstance();
		
		startCal.add(Calendar.WEEK_OF_YEAR, Integer.parseInt(startWeekNum)*-1);
		startCal.add(Calendar.DAY_OF_YEAR, Integer.parseInt(startDayNum)*-1);
		
		endCal.add(Calendar.WEEK_OF_YEAR, Integer.parseInt(endWeekNum));
		endCal.add(Calendar.DAY_OF_YEAR, Integer.parseInt(endDayNum));	
		
		logger.info("StartCal: "+startCal.getTime()+", EndCal: "+endCal.getTime());
	}
}
